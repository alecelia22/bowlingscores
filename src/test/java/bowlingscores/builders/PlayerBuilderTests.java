package bowlingscores.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.junit.Test;
import bowlingscores.exceptions.BusinessException;
import bowlingscores.model.Player;

public class PlayerBuilderTests {

    private static final String NAME = "TestName";

    @Test(expected = BusinessException.class)
    public void build_withEmptyValues_BusinessException() {
        new PlayerBuilder(NAME, new String[0]).build();
    }

    @Test
    public void build_withExtraValues() {
        assertThrows(
            PlayerBuilder.MORE_THROWS_ERROR,
            BusinessException.class,
            () -> new PlayerBuilder(NAME,
                    new String[]{"10", "3", "2", "1", "9", "10", "0", "10", "2", "8", "9", "F", "3", "1", "9", "1", "10", "1", "1", "2"})
                    .build());
    }

    @Test
    public void build_withLessValues() {
        assertThrows(PlayerBuilder.INVALID_INPUT,
                BusinessException.class,
                () -> new PlayerBuilder(NAME,
                        new String[]{"10", "3", "2", "1", "9", "10", "0", "10", "2", "8", "9", "F", "3", "1", "9", "1", "10"})
                        .build());
    }

    @Test
    public void build_allTen() {
        PlayerBuilder underTest = new PlayerBuilder(NAME,
                new String[]{"10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10"});
        Player actualPlayer = underTest.build();

        String[] expectedPinfalls =
                new String[]{"", "X", "", "X", "", "X", "", "X", "", "X", "", "X", "", "X", "", "X", "", "X", "X", "X", "X"};
        Integer expectedScore = 30;

        assertEquals(NAME, actualPlayer.getName());

        for (int i = 0; i < expectedPinfalls.length; i++) {
            assertEquals(expectedPinfalls[i], actualPlayer.getPinfalls()[i]);
        }

        for (Integer actual: actualPlayer.getScores()) {
            assertEquals(expectedScore, actual);
        }
    }

    @Test
    public void build_allZeroandFoul() {
        PlayerBuilder underTest = new PlayerBuilder(NAME,
                new String[]{"0", "0", "0", "0", "0", "0", "0", "0", "0", "F", "0", "0", "0", "0", "F", "0", "0", "0", "0", "0"});
        Player actualPlayer = underTest.build();

        String[] expectedPinfalls =
                new String[]{"0", "0", "0", "0", "0", "0", "0", "0", "0", "F", "0", "0", "0", "0", "F", "0", "0", "0", "0", "0"};
        Integer expectedScore = 0;

        assertEquals(NAME, actualPlayer.getName());

        for (int i = 0; i < expectedPinfalls.length; i++) {
            assertEquals(expectedPinfalls[i], actualPlayer.getPinfalls()[i]);
        }

        for (Integer actual: actualPlayer.getScores()) {
            assertEquals(expectedScore, actual);
        }
    }

    @Test
    public void build_withValues() {
        PlayerBuilder underTest = new PlayerBuilder(NAME,
                new String[]{"10", "3", "2", "1", "9", "10", "0", "10", "2", "8", "9", "F", "3", "1", "9", "1", "10", "1", "1"});
        Player actualPlayer = underTest.build();

        String[] expectedPinfalls =
                new String[]{"", "X", "3", "2", "1", "/", "", "X", "0", "/", "2", "/", "9", "F", "3", "1", "9", "/", "X", "1", "1"};
        Integer[] expectedScores =
                new Integer[]{15, 5, 20, 20, 12, 19, 9, 4, 20, 12};

        assertEquals(NAME, actualPlayer.getName());

        for (int i = 0; i < expectedPinfalls.length; i++) {
            assertEquals(expectedPinfalls[i], actualPlayer.getPinfalls()[i]);
        }

        for (int i = 0; i < expectedScores.length; i++) {
            assertEquals(expectedScores[i], actualPlayer.getScores()[i]);
        }
    }
}
