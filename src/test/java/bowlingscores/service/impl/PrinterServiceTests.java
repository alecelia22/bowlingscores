package bowlingscores.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import bowlingscores.builders.PlayerBuilder;
import bowlingscores.model.Player;
import bowlingscores.service.PrinterService;

public class PrinterServiceTests {

    private final PrinterService underTest;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    public PrinterServiceTests() {
        this.underTest = new PrinterServiceImpl();
    }

    @Test
    public void printFinalResults_withEmptyArray() {
        underTest.printFinalResults(new ArrayList<>());

        String expected = "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\n";

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void printFinalResults_withValues() {
        List<Player> players = Arrays.asList(
                new PlayerBuilder("Carl", new String[]{"10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10"}).build(),
                new PlayerBuilder("Jeff", new String[]{"10",  "7",  "3",  "9",  "0",  "10",  "0",  "8",  "8",  "2",  "F",  "6",  "10",  "10",  "10",  "8",  "1"}).build(),
                new PlayerBuilder("John", new String[]{"3",  "7",  "6",  "3",  "10",  "8",  "1",  "10",  "10",  "9",  "0",  "7",  "3",  "4",  "4",  "10",  "9",  "0"}).build(),
                new PlayerBuilder("Henry", new String[]{"0", "0", "0", "0", "0", "0", "0", "0", "0", "F", "0", "0", "0", "0", "F", "0", "0", "0", "0", "0"}).build()
        );

        underTest.printFinalResults(players);

        String expected =
                "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\n" +
                "Carl\n" +
                "Pinfalls\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\t\tX\tX\tX\tX\n" +
                "Score\t\t30\t\t60\t\t90\t\t120\t\t150\t\t180\t\t210\t\t240\t\t270\t\t300\n" +
                "Jeff\n" +
                "Pinfalls\t\tX\t7\t/\t9\t0\t\tX\t0\t8\t8\t/\tF\t6\t\tX\t\tX\tX\t8\t1\n" +
                "Score\t\t20\t\t39\t\t48\t\t66\t\t74\t\t84\t\t90\t\t120\t\t148\t\t167\n" +
                "John\n" +
                "Pinfalls\t3\t/\t6\t3\t\tX\t8\t1\t\tX\t\tX\t9\t0\t7\t/\t4\t4\tX\t9\t0\n" +
                "Score\t\t16\t\t25\t\t44\t\t53\t\t82\t\t101\t\t110\t\t124\t\t132\t\t151\n" +
                "Henry\n" +
                "Pinfalls\t0\t0\t0\t0\t0\t0\t0\t0\t0\tF\t0\t0\t0\t0\tF\t0\t0\t0\t0\t0\n" +
                "Score\t\t0\t\t0\t\t0\t\t0\t\t0\t\t0\t\t0\t\t0\t\t0\t\t0\n";

        assertEquals(expected, outContent.toString());
    }
}
