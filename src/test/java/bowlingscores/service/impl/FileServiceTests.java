package bowlingscores.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import org.junit.Test;
import bowlingscores.exceptions.BusinessException;
import bowlingscores.service.FileService;

public class FileServiceTests {

    private final FileService underTest;

    public FileServiceTests () {
        this.underTest = new FileServiceImpl();
    }

    @Test
    public void fileToMap_invalidFile() {
        assertThrows(FileServiceImpl.INVALID_FILE_PATH,
                BusinessException.class,
                () -> underTest.fileToMap("fake.txt"));
    }

    @Test
    public void fileToMap_withNullValue() {
        assertThrows(FileServiceImpl.INVALID_FILE_PATH,
                BusinessException.class,
                () -> underTest.fileToMap(null));
    }

    @Test
    public void fileToMap_withNegativePinfall() {
        String expectedMessage = String.format(FileServiceImpl.INVALID_VALUE, -1);

        assertThrows(expectedMessage,
                BusinessException.class,
                () -> underTest.fileToMap("src/test/resources/negativePinfallInput.txt"));
    }

    @Test
    public void fileToMap_withLetterAsPinfall() {
        String expectedMessage = String.format(FileServiceImpl.INVALID_VALUE, -1);

        assertThrows(expectedMessage,
                BusinessException.class,
                () -> underTest.fileToMap("src/test/resources/letterPinfallInput.txt"));
    }

    @Test
    public void fileToMap_withValidInput() {
        Map<String, List<String>> actual = underTest.fileToMap("src/test/resources/testInput.txt");

        assertEquals(4, actual.size());

        assertTrue(actual.containsKey("Jeff"));
        assertTrue(actual.containsKey("John"));
        assertTrue(actual.containsKey("Carl"));
        assertTrue(actual.containsKey("Henry"));


        String actualJeff = actual.get("Jeff").toString();
        assertEquals("[10, 7, 3, 9, 0, 10, 0, 8, 8, 2, F, 6, 10, 10, 10, 8, 1]", actualJeff);

        String actualJohn = actual.get("John").toString();
        assertEquals("[3, 7, 6, 3, 10, 8, 1, 10, 10, 9, 0, 7, 3, 4, 4, 10, 9, 0]", actualJohn);

        List<String> actualCarl = actual.get("Carl");
        actualCarl.forEach(c -> assertEquals("10", c));

        List<String> actualHenry = actual.get("Henry");
        actualHenry.forEach(h -> assertEquals("0", h));
    }
}
