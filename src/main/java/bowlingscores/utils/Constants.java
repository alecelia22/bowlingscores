package bowlingscores.utils;

public class Constants {

    public static final String TAB = "\t";

    public static final String FRAME = "Frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10";

    public static final String FOUL = "F";
}
