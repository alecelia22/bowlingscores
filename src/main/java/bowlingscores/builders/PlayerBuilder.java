package bowlingscores.builders;

import java.util.Optional;
import bowlingscores.exceptions.BusinessException;
import bowlingscores.model.Player;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.primitives.Ints;

public class PlayerBuilder {

    private static final String STRIKE = "X";
    private static final String SPARE = "/";
    private static final Integer MAX_PINFALLS = 10;

    @VisibleForTesting
    protected static final String MORE_THROWS_ERROR = "Player has more throws than allowed";
    @VisibleForTesting
    protected static final String INVALID_INPUT = "Invalid file input information";

    // Player name
    private final String name;
    // Raw pinfalls
    private final String[] rawPinfalls;

    // Player pinfalls
    private String[] pinfalls;
    // Player scores
    private Integer[] scores;

    // Helper values
    private int position;
    private int scorePosition;

    public PlayerBuilder(String name, String[] rawPinfalls) {
        this.name = name;
        this.rawPinfalls = rawPinfalls;
    }

    public Player build() {
        // Init values to make calculations
        position = 0;
        pinfalls = new String[21];
        scorePosition = 0;
        scores = new Integer[10];

        try {
            // Use the raw list to get player pinfalls and scores
            calculatePinfallsAndScores();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new BusinessException(INVALID_INPUT);
        }

        // Validate amount of games
        validateGames();

        // Create an inmutable player
        return new Player(name, pinfalls, scores);
    }

    private void validateGames() {
        if (scores[9] == null) {
            throw new BusinessException("Invalid amount of game for player " + name);
        }
    }

    private void calculatePinfallsAndScores() {
        for (int i = 0; i < rawPinfalls.length; i++) {
            // Last Game, that its able to have 3 throws
            if (position >= 18) {
                calculateLastGame(i);

            // Games 1 to 9
            } else {
                if ((position% 2) == 0) {
                    calculateEvenThrow(i);
                } else {
                    calculateOddThrow(i);
                }
            }

            position++;
        }
    }

    private void calculateOddThrow(int i) {
        if (isSpare(pinfalls[position-1], rawPinfalls[i])) {
            pinfalls[position] = SPARE;
            // 10 points plus next value
            scores[scorePosition++] = sum(MAX_PINFALLS.toString(), rawPinfalls[i+1]);
        } else {
            pinfalls[position] = rawPinfalls[i];
            // This position plus previus
            scores[scorePosition++] = sum(rawPinfalls[i], rawPinfalls[i-1]);
        }
    }

    private void calculateEvenThrow(int i) {
        if (isStrike(rawPinfalls[i])) {
            // Set game pinfalls
            pinfalls[position] = "";
            pinfalls[++position] = STRIKE;

            // 10 points plus the next two values
            scores[scorePosition++] = sum(MAX_PINFALLS.toString(), rawPinfalls[i+1], rawPinfalls[i+2]);
        } else {
            pinfalls[position] = rawPinfalls[i];
        }
    }

    /**
     * Last Game has it's own behavior, so I create a method only for it
     */
    private void calculateLastGame(int i) {
        switch (position) {
            case 18:
                if (isStrike(rawPinfalls[i])) {
                    pinfalls[position] = STRIKE;
                    // 10 points plus the next two values
                    scores[scorePosition++] = sum(MAX_PINFALLS.toString(), rawPinfalls[i + 1], rawPinfalls[i + 2]);
                } else {
                    pinfalls[position] = rawPinfalls[i];
                }
                break;
            case 19:
                // Can be a strike on previues throw and cero in this
                if (!"0".equals(rawPinfalls[i]) && isSpare(rawPinfalls[i-1], rawPinfalls[i])) {
                    pinfalls[position] = SPARE;
                    // 10 points plus next value
                    scores[scorePosition++] = sum(MAX_PINFALLS.toString(), rawPinfalls[i+1]);

                } else if (MAX_PINFALLS.toString().equals(rawPinfalls[i])) {
                    pinfalls[position] = STRIKE;

                } else {
                    pinfalls[position] = rawPinfalls[i];

                    if (!isStrike(rawPinfalls[i-1])) {
                        // This position plus previus
                        scores[scorePosition++] = sum(rawPinfalls[i], rawPinfalls[i-1]);
                    }
                }
                break;
            case 20:
                if (MAX_PINFALLS.toString().equals(rawPinfalls[i])) {
                    pinfalls[position] = STRIKE;
                } else {
                    pinfalls[position] = rawPinfalls[i];
                }
                break;
            default:
                throw new BusinessException(MORE_THROWS_ERROR);
        }
    }

    private static Integer sum(String value1, String value2) {
        return sum(value1, value2, null);
    }

    private static Integer sum(String value1, String value2, String value3) {
        return Optional.ofNullable(value1).map(Ints::tryParse).orElse(0) +
                Optional.ofNullable(value2).map(Ints::tryParse).orElse(0) +
                Optional.ofNullable(value3).map(Ints::tryParse).orElse(0);
    }

    private static boolean isSpare(String firstThrow, String secondThrow) {
        return Optional.ofNullable(firstThrow).map(Ints::tryParse).orElse(0) +
                Optional.ofNullable(secondThrow).map(Ints::tryParse).orElse(0)
                == MAX_PINFALLS;
    }

    private static boolean isStrike(String pinfalls) {
        return Optional.ofNullable(pinfalls).map(Ints::tryParse).orElse(0) == MAX_PINFALLS;
    }
}
