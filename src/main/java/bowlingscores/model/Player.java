package bowlingscores.model;

public class Player {

    private final String name;
    private final String[] pinfalls;
    private final Integer[] scores;

    public Player(String name, String[] pinfalls, Integer[] scores) {
        this.name = name;
        this.pinfalls = pinfalls;
        this.scores = scores;
    }

    public String getName() {
        return name;
    }

    public Integer[] getScores() {
        return scores;
    }

    public String[] getPinfalls() {
        return pinfalls;
    }

}
