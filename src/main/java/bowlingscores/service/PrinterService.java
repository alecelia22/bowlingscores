package bowlingscores.service;

import java.util.List;
import bowlingscores.model.Player;

public interface PrinterService {
    void printFinalResults(List<Player> players);
}
