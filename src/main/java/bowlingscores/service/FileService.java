package bowlingscores.service;

import java.util.List;
import java.util.Map;

public interface FileService {
    Map<String, List<String>> fileToMap(String path);
}
