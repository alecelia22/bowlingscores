package bowlingscores.service.impl;

import java.util.List;
import bowlingscores.model.Player;
import bowlingscores.service.PrinterService;
import bowlingscores.utils.Constants;

public class PrinterServiceImpl implements PrinterService {

    public void printFinalResults(List<Player> players) {
        //Print players information
        System.out.println(Constants.FRAME);

        players.forEach(player -> {
            // Line with Player Name
            System.out.println(player.getName());

            // Line with Pinfalls
            StringBuilder pinfallsLine = new StringBuilder("Pinfalls").append(Constants.TAB);
            for (String pinfall: player.getPinfalls()) {
                if (pinfall != null) {
                    pinfallsLine.append(pinfall).append(Constants.TAB);
                }
            }
            System.out.println(pinfallsLine.toString().trim());

            // Line with Scores
            StringBuilder scoresLine = new StringBuilder("Score").append(Constants.TAB).append(Constants.TAB);
            int totalScore = 0;
            for (Integer score: player.getScores()) {
                totalScore += score;
                scoresLine.append(totalScore).append(Constants.TAB).append(Constants.TAB);
            }
            System.out.println(scoresLine.toString().trim());
        });
    }
}
