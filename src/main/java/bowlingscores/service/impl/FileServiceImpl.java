package bowlingscores.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import bowlingscores.exceptions.BusinessException;
import bowlingscores.service.FileService;
import bowlingscores.utils.Constants;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.primitives.Ints;

public class FileServiceImpl implements FileService {

    @VisibleForTesting
    protected static final String INVALID_FILE_PATH = "Invalid file path";
    @VisibleForTesting
    protected static final String INVALID_VALUE = "Invalid value '%s'. Pinfalls must be a number between 0 and 10 or F (foul)";

    @Override
    public Map<String, List<String>> fileToMap(String path) {
        Map<String, List<String>> records = new HashMap<>();

        // Read file information
        try (Stream<String> stream = Files.lines(Paths.get(path))) {

            stream.forEach(line -> {
                String name = line.split(Constants.TAB)[0];
                String pinfalls = line.split(Constants.TAB)[1];
                validatePinfalls(pinfalls);

                if (records.containsKey(name)) {
                    records.get(name).add(pinfalls);
                } else {
                    records.put(name, new ArrayList<>(Collections.singletonList(pinfalls)));
                }
            });

        } catch (IOException | NullPointerException e) {
            throw new BusinessException(INVALID_FILE_PATH);
        }

        return records;
    }

    private void validatePinfalls(String pinfalls) {
        if (!Constants.FOUL.equalsIgnoreCase(pinfalls)) {
            int number = Optional.ofNullable(pinfalls).map(Ints::tryParse).orElse(-1);

            if (number < 0 || number > 10) {
                throw new BusinessException(String.format(INVALID_VALUE, pinfalls));
            }
        }
    }
}
