package bowlingscores;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import bowlingscores.builders.PlayerBuilder;
import bowlingscores.model.Player;
import bowlingscores.service.FileService;
import bowlingscores.service.PrinterService;
import bowlingscores.service.impl.FileServiceImpl;
import bowlingscores.service.impl.PrinterServiceImpl;

public class GameTracking {

    private final PrinterService printerService;
    private final FileService fileService;

    public GameTracking() {
        this.printerService = new PrinterServiceImpl();
        this.fileService = new FileServiceImpl();
    }

    public void processFile(String path) {
        // Make a map with the file information
        Map<String, List<String>> records = fileService.fileToMap(path);

        // Make a list of players from the file information
        List<Player> players = new ArrayList<>();
        records.forEach((name, rawPinfalls) ->
                players.add(
                        // Build player with the file info
                        new PlayerBuilder(name, rawPinfalls.toArray(new String[0]))
                                .build()
                )
        );

        // Print results
        printerService.printFinalResults(players);
    }
}
