#Bowling Scores
This is a gradle java project.  
This program use a file as input and return the bowling match results. 

##Build
First time you run this project, you have to restore the gradle wrapper by running:  
```gradle wrapper```

After that, It builds with a ```./gradlew build``` command 

##Run
This projects runs with gradle and takes a file path as only argument.  

```./gradlew run --args=FILE_PATH```

So if you want to run a file call input.txt and it is in the same path that the project, you can use the folowing command line:  
```./gradlew run --args=input.txt```

Or you can use the whole path, example:  
```./gradlew run --args=/users/myuser/documents/input.txt```

#Run as executable file
The project cointains the last version build as a jar on /build/libs/  
So, it can be run as a jar:
```java -jar /build/libs/bowlingscores.jar FILE_PATH```

Examples:  
```java -jar /build/libs/bowlingscores.jar input.txt```
```java -jar /build/libs/bowlingscores.jar /users/myuser/documents/input.txt```


Note: Tests only works on Unix environment, if you want to run it on windows, '\n' should be replace with '\r\n'. Or, as a project improvement it could be use docker to build the project.    
